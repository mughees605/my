import React, {Component} from "react";
import "./TemplateEditor.css";
import {
    Col,
    Panel,
    Row,
    FormGroup,
    ControlLabel,
    FormControl,
    FieldGroup
} from "react-bootstrap";

import {Editor} from "react-draft-wysiwyg"
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

class TemplateEditor extends Component {

    render() {
        return (
            <div className="editor-container">
                <Row>
                    <Col md={8} className="first-col">
                        <div className="first-col-head">
                            <h1 className='template-editor'>TemplateEditor</h1>
                        </div>
                        <Panel className="panel">
                            <Col md={10} mdOffset={1}>
                                <h3 className='title'>Select template</h3>
                                <FormGroup controlId="formControlsSelect">
                                    <FormControl className='dropdown' componentClass="select" placeholder="select">
                                        <option value="select">Into Email</option>
                                        <option value="other">...</option>
                                    </FormControl>
                                    <span>
                                        <p className='notice'>Note: for internal use only</p>
                                    </span>
                                </FormGroup>
                                <h3 className='title'>Subject:</h3>
                                <FormGroup>
                                    <FormControl
                                        className='subject-editor'
                                        type="text"
                                        placeholder="Hi {{first_name}}, I want to help {{company_name}} launch your mobile app"/>
                                </FormGroup>
                                <h3 className='title'>Content:</h3>
                                <div
                                    style={{
                                    backgroundColor: "#fff"
                                }}>
                                    <Editor
                                        inline={{
                                        inDropdown: false,
                                        options: [
                                            'bold',
                                            'italic',
                                            'underline',
                                            'strikethrough',
                                        ],
                                        
                                    }}/> {/*<Editor/>*/}
                                </div>

                            </Col>
                        </Panel>
                    </Col>
                </Row>
            </div>
        )
    }
}
export default TemplateEditor;